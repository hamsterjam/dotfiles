set nocompatible

"
" Plugins
"
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'gmarik/Vundle.vim'

" Simple binds
Plugin 'tpope/vim-unimpaired'

" Editing
Plugin 'tpope/vim-repeat'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-commentary'
Plugin 'tpope/vim-speeddating'
Plugin 'dhruvasagar/vim-table-mode'

" Git
Plugin 'tpope/vim-fugitive'
Plugin 'shumphrey/fugitive-gitlab.vim'

" External commands
Plugin 'tpope/vim-dispatch'
Plugin 'tpope/vim-eunuch'
Plugin 'mechatroner/minimal_gdb'

" Color schemes
Plugin 'morhetz/gruvbox'

" Highlighting
Plugin 'beyondmarc/glsl.vim'
Plugin 'pangloss/vim-javascript'
Plugin 'hamsterjam/vim-gcovered'
Plugin 'vim-scripts/ShaderHighLight'
Plugin 'jwalton512/vim-blade'
Plugin 'posva/vim-vue'

call vundle#end()

" Configuration
au VimEnter * :SpeedDatingFormat %Y / %m / %d

let g:table_mode_corner='|'

"
" Colors and visual things
"
set background=dark
syntax on

if has("gui_running")
    set guioptions-=m
    set guioptions-=T
    set guioptions-=r
    set guioptions-=L
    set guioptions-=e
    set guifont=inconsolata\ 10

    set guiheadroom=0

    colorscheme gruvbox
else
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    set termguicolors
    let g:gruvbox_guisp_fallback = "bg"
    colorscheme gruvbox

    "highlight Normal ctermbg=none
    highlight Comment cterm=italic
endif

let g:tex_flavor = "latex"
set hlsearch
set conceallevel=0

"
" UI config
"
set backspace=indent,eol,start
set mouse=a
set number
set nowrap
set showcmd
set nohidden
set laststatus=2
set relativenumber

" Splits
set splitbelow
set splitright

set wildmenu
set wildmode=full

set directory=~/.swp

"
" File System stuff
"
set path+=** "Recursive search with :find

let g:netrw_banner=0 "Remove the banner
let g:netrw_altv=1   "Open splits to the right
let g:netrw_liststyle=3 "Tree view
let g:netrw_list_hide=netrw_gitignore#Hide() . ",.*\.meta"

"
" Indentation Options
"
set tabstop=4
set shiftwidth=4
set softtabstop=-1
set expandtab
filetype plugin indent on

set list
set listchars=tab:>-

"
" Folding
"
set foldenable
set foldlevelstart=10
set foldnestmax=10
set foldmethod=manual
nnoremap <space> za
vnoremap <space> zf

"
" Mappings
"

" Unbind arrow keys and PgUp PgDn
nnoremap <up>       <nop>
nnoremap <down>     <nop>
nnoremap <left>     <nop>
nnoremap <right>    <nop>
nnoremap <pageup>   <nop>
nnoremap <pagedown> <nop>

" Generaly useful bindings
nnoremap <silent> ,/ :let@/=""<CR>
nnoremap <silent> ;; A;<ESC>
nnoremap <silent> ~ ~h
nnoremap <F2>  :Rename<Space>
nnoremap <F9>  :!./
nnoremap <F10> :!
nnoremap <F3>  :find<Space>
nnoremap <F4>  :tag<Space>

" I always open this accidentaly
nnoremap q: <nop>

" Tab Navigation on the home row
nnoremap <silent> <C-h> gT
nnoremap <silent> <C-l> gt

"<C-y> is clobbered by my system copy paste
nnoremap <C-k> <C-y>
nnoremap <C-j> <C-e>

" Navigate popup menus (such as C-N) with C-J and C-K
inoremap <expr> <C-J> pumvisible() ? "\<C-N>" : "<C-J>"
inoremap <expr> <C-K> pumvisible() ? "\<C-P>" : "<C-K>"

" Unimpaired style bindings for moving tabs (windows)
nnoremap <silent> [w :-tabmove<CR>
nnoremap <silent> ]w :+tabmove<CR>
nnoremap <silent> [W :0tabmove<CR>
nnoremap <silent> ]W :$tabmove<CR>

" System copy/paste for WSL
if executable('/mnt/c/Windows/System32/clip.exe')
    nnoremap <silent> <C-y> :call system('/mnt/c/Windows/System32/clip.exe', @0)<CR>
    nnoremap <silent> <C-p> :let @" = system('powershell.exe -Command Get-Clipboard')<CR>
end
"
" Auto Commands
"

" Removes trailing whitespace
autocmd BufRead,BufWrite * if ! &bin | silent! %s/\s\+$//ge | endif

" Show relative number only on active buffer
:augroup numbertoggle
:  autocmd!
:  autocmd WinEnter,BufEnter,FocusGained * set relativenumber
:  autocmd WinLeave,BufLeave,FocusLost   * set norelativenumber
:augroup END

" Delete last quickfix item when you run make
function! CleanQF()
    let qf = getqflist()
    let i = len(qf) - 1
    while i >= 0
        let buf = qf[i].bufnr
        if buf == 0
            " call remove(qf, i)
        elseif !filereadable(bufname(buf))
            call remove(qf, i)
            execute 'bwipeout' buf
        endif
        let i -= 1
    endwhile
    call setqflist(qf, 'r')
endfunction

autocmd QuickFixCmdPost * call CleanQF()
