#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

export NO_AT_BRIDGE=1

# Get colours working properly
if [[ $TERM == "xterm" ]]; then
    export TERM=xterm-256color
elif [[ $TERM == "tmux" ]]; then
    export TERM=tmux-256color
fi

# Terminal prompt
PS1='[\u: \W]$ '

# Default editor
export VISUAL=vim
export EDITOR=vim

# qt4 ibus bullshit
export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus

# Scripts
alias kill-name="$HOME/Scripts/kill-name.sh"
alias jspre="$HOME/Scripts/jspre.sh"
alias toggle-touchpad="$HOME/Scripts/toggle-touchpad.sh"
alias sloc="$HOME/Scripts/sloc-c.sh"
alias reqcat="$HOME/Scripts/reqcat.sh"
alias 4chan-image="$HOME/Scripts/4chan-image-save.sh"
alias rand="$HOME/Scripts/rand.sh"

# Vimish aliases
alias  :w="echo \"What are you trying to save?\""
alias  :q="echo \"This isn't vim you idiot, try Ctrl+D\""
alias :qa="echo \"This isn't vim you idiot, try Ctrl+D\""

# Some simple functions
alias test-server="python3 -m http.server 8000"
alias sizeof='stat --printf="%s bytes.\n"'
alias please='sudo "$BASH" -c "$(history -p !!)"'

# Modified versions of built ins
alias lsa='ls -a'
mkcd () {
    mkdir $1 && cd $1
}

# This is the very best
extract () {
   if [ -f $1 ] ; then
       case $1 in
           *.tar.bz2)   tar xvjf $1    ;;
           *.tar.gz)    tar xvzf $1    ;;
           *.bz2)       bunzip2 $1     ;;
           *.rar)       unrar x $1     ;;
           *.gz)        gunzip $1      ;;
           *.tar)       tar xvf $1     ;;
           *.tbz2)      tar xvjf $1    ;;
           *.tgz)       tar xvzf $1    ;;
           *.zip)       unzip $1       ;;
           *.Z)         uncompress $1  ;;
           *.7z)        7z x $1        ;;
           *)           echo "don't know how to extract '$1'..." ;;
       esac
   else
       echo "'$1' is not a valid file!"
   fi
 }

# Specifically for doing git actions on my dotfiles
alias git-dot="git --git-dir ~/dotfiles.git --work-tree ~"

# Some stuff for working with csv files
csvawk () {
    argArray=( $@ )
    len=${#argArray[@]}
    file=${argArray[$len-1]}
    args=${argArray[@]:0:$len-1}
    sed 's/^\s*"//;s/"\s*$//' "$file" | awk -F '","' '(NR == 1) {for (i = 1; i <= NF; i++) col[$i] = i};'"${args[@]}"
}

csvindex () {
    csvawk '(NR == 1) {for (i = 1; i <= NF; i++) print i":", $i}' "$1" | column -t
}

csvsearch() {
    for folder in *; do
        file="$folder/$2"
        if [ -e "$file" ]; then
            result=$(csvawk "$1" "$file")
            if [ "$result" != "" ]; then
                echo "$folder":
                echo "$result"
            fi
        fi
    done
}
